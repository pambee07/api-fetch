async function getCharacters() {
  try {
    // appelle l'API et récupère la réponse
    const response = await fetch("https://jsonplaceholder.typicode.com/users");

    // de la réponse, on garde uniquement le JSON, qui sera converti en JavaScript
    const characterList = await response.json();

    // pour chaque personnage du tableau
    for (const oneCharacter of characterList) {
      // on affiche le nom du personnage
      console.log("Nom du personnage:", oneCharacter.name);

      // on accède aux données d'adresse du personnage
      const address = oneCharacter.address;

      // on affiche la rue du personnage
      console.log("Rue du personnage:", address.street);

      // on affiche le code postal du personnage
      console.log("Code postal du personnage:", address.zipcode);

      // on affiche la ville du personnage
      console.log("Ville du personnage:", address.city);

      // Ajoutez d'autres informations que vous souhaitez afficher
    }
  } catch (error) {
    console.error("Erreur lors de la récupération des personnages:", error);
  }
}

// on appelle la fonction getCharacters
getCharacters();
